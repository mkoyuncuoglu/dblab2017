#initial time: 0.503
#alter index: 0.438
#after PK: 0.102


USE protein_indexes;
SELECT * FROM proteins;

LOAD DATA LOCAL INFILE 'D:\insert.txt' INTO table Proteins fields terminated by '|';

SELECT * FROM proteins WHERE accession ="A0A0P0WPH4";

SELECT accession,uniprot_id,protein_name,organism
FROM proteins
WHERE protein_name LIKE "%tumor%" AND uniprot_id LIKE "%human%"
ORDER BY uniprot_id;

CREATE INDEX uniprot_index ON proteins(uniprot_id);

drop index uniprot_index on proteins;

ALTER TABLE proteins ADD CONSTRAINT pk_proteins PRIMARY KEY (uniprot_id);