USE movie_db;


create VIEW titles_directors AS 
SELECT title,director_name
FROM movies,movie_directors,directors
WHERE movies.movie_id=movie_directors.movie_id AND directors.director_id=movie_directors.director_id;


SELECT title FROM titles_directors;


CREATE VIEW action_movies AS 
SELECT movies.movie_id , movies.title
FROM movies JOIN genres ON movies.movie_id=genres.movie_id
WHERE genre_name LIKE "%action%";

SELECT * FROM action_movies;

CALL GetMovies();
CALL 'movie_db'.'GetMovieByDirector'("Peter Jackson");	
CALL 'GetMovieByCoStars'("Ian McKellen","Orlando Bloom");
